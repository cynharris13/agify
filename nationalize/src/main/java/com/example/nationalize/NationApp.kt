package com.example.nationalize

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Nation app.
 *
 * @constructor Create empty Nation app
 */
@HiltAndroidApp
class NationApp : Application()

package com.example.nationalize.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nationalize.model.NationRepo
import com.example.nationalize.view.response.NationState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Nation view model.
 *
 * @property nationRepo
 * @constructor Create empty Nation view model
 */
@HiltViewModel
class NationViewModel @Inject constructor(private val nationRepo: NationRepo) : ViewModel() {
    private val _state = MutableLiveData(NationState())
    val state: LiveData<NationState> get() = _state

    /**
     * Get nation.
     *
     * @param name
     */
    fun getNation(name: String) = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val nationality = nationRepo.getNationality(name)
        _state.value = _state.value?.copy(isLoading = false, nationData = nationality)
    }
}

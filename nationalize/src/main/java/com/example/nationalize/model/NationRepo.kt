package com.example.nationalize.model

import com.example.nationalize.model.entity.Country
import com.example.nationalize.model.entity.Nationality
import com.example.nationalize.model.remote.NationService
import javax.inject.Inject

/**
 * Nation repo.
 *
 * @property nationService
 * @constructor Create empty Nation repo
 */
class NationRepo @Inject constructor(private val nationService: NationService) {
    /**
     * Get nationality.
     *
     * @param name
     * @return
     */
    suspend fun getNationality(name: String): Nationality {
        val response = nationService.getNation(name)
        return if (response.isSuccessful) {
            val nationDto = response.body()!!
            Nationality(
                name = nationDto.name,
                country = nationDto.country.map { Country(it.countryId, it.probability) }
            )
        } else { Nationality() }
    }
}

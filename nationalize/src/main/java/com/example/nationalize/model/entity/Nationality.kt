package com.example.nationalize.model.entity

/**
 * Nationality object.
 *
 * @property country
 * @property name
 * @constructor Create empty Nationality
 */
data class Nationality(
    val country: List<Country> = emptyList(),
    val name: String = ""
)

package com.example.nationalize.model.dto

@kotlinx.serialization.Serializable
data class NationResponse(
    val country: List<CountryDTO>,
    val name: String
)

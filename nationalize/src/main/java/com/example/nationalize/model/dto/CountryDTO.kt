package com.example.nationalize.model.dto

import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
data class CountryDTO(
    @SerialName("country_id") val countryId: String,
    val probability: Double
)

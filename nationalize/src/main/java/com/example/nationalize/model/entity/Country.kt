package com.example.nationalize.model.entity

/**
 * Country object.
 *
 * @property countryId
 * @property probability
 * @constructor Create empty Country
 */
data class Country(
    val countryId: String,
    val probability: Double
)

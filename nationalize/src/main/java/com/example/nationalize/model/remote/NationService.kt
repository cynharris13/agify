package com.example.nationalize.model.remote

import com.example.nationalize.model.dto.NationResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Nation service.
 *
 * @constructor Create empty Nation service
 */
interface NationService {
    @GET(".")
    suspend fun getNation(@Query("name") name: String): Response<NationResponse>
}

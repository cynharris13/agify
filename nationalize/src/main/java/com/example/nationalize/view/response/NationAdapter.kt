package com.example.nationalize.view.response

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nationalize.databinding.SingleNationalityBinding
import com.example.nationalize.model.entity.Country

/**
 * Nation adapter.
 *
 * @constructor Create empty Nation adapter
 */
class NationAdapter : RecyclerView.Adapter<NationAdapter.ViewHolder>() {
    private val countries: MutableList<Country> = mutableListOf()

    /**
     * View holder.
     *
     * @property binding
     * @constructor Create empty View holder
     */
    inner class ViewHolder(private val binding: SingleNationalityBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Bind nation.
         *
         * @param country
         */
        fun bindNation(country: Country) = with(binding) {
            val countryText = "${country.countryId}: ${(country.probability * HUNDRED).toInt()}% chance"
            textview.text = countryText
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = SingleNationalityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindNation(countries[position])
    }

    override fun getItemCount(): Int = countries.size

    /**
     * Add countries.
     *
     * @param countryList
     */
    fun addCountries(countryList: List<Country>) {
        val amount = itemCount
        countries.addAll(countryList)
        notifyItemRangeInserted(amount, countryList.size)
    }
    companion object {
        const val HUNDRED = 100
    }
}

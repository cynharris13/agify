package com.example.nationalize.view.response

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nationalize.databinding.FragmentSecondBinding
import com.example.nationalize.viewmodel.NationViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@AndroidEntryPoint
class ResponseFragment : Fragment() {
    private val nationViewModel by viewModels<NationViewModel>()

    private val name by lazy { arguments?.getString("name") ?: "" }

    private val adapter by lazy { NationAdapter() }

    private var _binding: FragmentSecondBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSecondBinding.inflate(inflater, container, false)

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(requireContext())

        binding.rvlist.layoutManager = layoutManager

        nationViewModel.getNation(name)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nationViewModel.state.observe(viewLifecycleOwner) {
            if (it.nationData != null) {
                binding.nameView.text = it.nationData.name
                adapter.addCountries(it.nationData.country)
                binding.rvlist.adapter = adapter
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

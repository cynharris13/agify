package com.example.nationalize.view.response

import com.example.nationalize.model.entity.Nationality

/**
 * Nation state.
 *
 * @property isLoading
 * @property nationData
 * @constructor Create empty Nation state
 */
data class NationState(
    val isLoading: Boolean = false,
    val nationData: Nationality? = null
)

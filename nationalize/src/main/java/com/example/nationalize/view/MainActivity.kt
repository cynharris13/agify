package com.example.nationalize.view

import androidx.appcompat.app.AppCompatActivity
import com.example.nationalize.R
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)

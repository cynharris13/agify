package com.example.nationalize.viewmodel

import com.example.nationalize.model.NationRepo
import com.example.nationalize.model.entity.Nationality
import com.example.nationalize.util.CoroutinesTestExtension
import com.example.nationalize.util.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class NationViewModelTest {
    private val repo = mockk<NationRepo>()
    private val nationVM = NationViewModel(repo)

    @Test
    fun getNation() = runTest {
        // given
        val expected = Nationality(name = "lol")
        coEvery { repo.getNationality("lol") } coAnswers { expected }

        // when
        nationVM.getNation("lol")

        // then
        Assertions.assertFalse(nationVM.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, nationVM.state.value?.nationData)
    }
}

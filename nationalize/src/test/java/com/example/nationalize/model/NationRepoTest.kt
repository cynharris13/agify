package com.example.nationalize.model

import com.example.nationalize.model.dto.CountryDTO
import com.example.nationalize.model.dto.NationResponse
import com.example.nationalize.model.entity.Nationality
import com.example.nationalize.model.remote.NationService
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@OptIn(ExperimentalCoroutinesApi::class)
internal class NationRepoTest {
    private val service = mockk<NationService>()
    private val repo = NationRepo(service)

    @Test
    fun getNationality() = runTest {
        // given
        val dto = NationResponse(emptyList(), "lol")
        coEvery { service.getNation("lol").isSuccessful } coAnswers { true }
        coEvery { service.getNation("lol").body() } coAnswers { dto }
        val expected = Nationality(name = "lol")

        // when
        val actual = repo.getNationality("lol")

        // then
        Assertions.assertEquals(expected, actual)
    }
}

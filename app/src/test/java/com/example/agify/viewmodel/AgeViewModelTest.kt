package com.example.agify.viewmodel

import com.example.agify.model.AgeRepo
import com.example.agify.model.dto.AgeResponse
import com.example.agify.util.CoroutinesTestExtension
import com.example.agify.util.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class AgeViewModelTest {
    val repo = mockk<AgeRepo>()
    val ageViewModel = AgeViewModel(repo)

    @Test
    fun getAge() = runTest {
        // given
        val expected = AgeResponse(123_456, 1, "lynne")
        coEvery { repo.getAge("lynne") } coAnswers { expected }

        // when
        ageViewModel.getAge("lynne")

        // then
        Assertions.assertFalse(ageViewModel.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, ageViewModel.state.value?.ageData)
    }
}

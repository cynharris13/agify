package com.example.agify.model

import com.example.agify.model.dto.AgeResponse
import com.example.agify.model.remote.AgeService
import com.example.agify.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class AgeRepoTest {
    private val service = mockk<AgeService>()
    val repo = AgeRepo(service)

    @Test
    fun testGetAge() = runTest {
        // given
        val expected = AgeResponse(123_456, 1, "lynne")
        coEvery { service.getAge("lynne") } coAnswers { expected }

        // when
        val actual = repo.getAge("lynne")

        // then
        Assertions.assertEquals(expected, actual)
    }
}

package com.example.agify.model.dto

import kotlinx.serialization.Serializable

@Serializable
data class AgeResponse(
    val age: Int = 0,
    val count: Int = 0,
    val name: String = ""
)

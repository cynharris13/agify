package com.example.agify.model.remote

import com.example.agify.model.dto.AgeResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Age service.
 *
 * @constructor Create empty Age service
 */
interface AgeService {
    @GET(".")
    suspend fun getAge(@Query("name") name: String): AgeResponse
}

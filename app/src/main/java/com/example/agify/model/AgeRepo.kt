package com.example.agify.model

import com.example.agify.model.dto.AgeResponse
import com.example.agify.model.remote.AgeService
import javax.inject.Inject

/**
 * Age repository.
 *
 * @property ageService
 * @constructor Create empty Age repo
 */
class AgeRepo @Inject constructor(private val ageService: AgeService) {
    // private val theTag = "AgeRepo"

    /**
     * Get age.
     *
     * @param name
     * @return
     */
    suspend fun getAge(name: String): AgeResponse {
        // Log.e(theTag, "getAge: searching for $name")
        // Log.e(theTag, "getAge: Miraculous success!!!! $response")
        return ageService.getAge(name)
    }
}

package com.example.agify.view.age

import com.example.agify.model.dto.AgeResponse

/**
 * Age state.
 *
 * @property isLoading
 * @property ageData
 * @constructor Create empty Age state
 */
data class AgeState(
    val isLoading: Boolean = false,
    val ageData: AgeResponse? = null
)

package com.example.agify.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.agify.R
import com.example.agify.databinding.FragmentFirstBinding
import com.example.agify.viewmodel.AgeViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class HomeFragment : Fragment() {
    // private val theTag = "HomeFragment"
    private var _binding: FragmentFirstBinding? = null

    private val ageViewModel by activityViewModels<AgeViewModel>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonFirst.setOnClickListener {
            val text = binding.input.editText?.text?.toString()?.trim() ?: ""
            ageViewModel.getAge(text)
            // Log.e(theTag, "onViewCreated: text was $text ; Navigating away from home fragment. ")
            findNavController().navigate(R.id.action_FirstFragment_to_ageFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

package com.example.agify.view.age

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.agify.databinding.FragmentAgeBinding
import com.example.agify.viewmodel.AgeViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class AgeFragment : Fragment() {
    // private val theTag = "AgeFragment"
    private val ageViewModel by activityViewModels<AgeViewModel>()

    private var _binding: FragmentAgeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAgeBinding.inflate(inflater, container, false)
        // val text = ageViewModel.state.value?.ageData
        // Log.e(theTag, "onCreateView: In AGE Fragment, the age was $text")
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ageViewModel.state.observe(viewLifecycleOwner) {
            // Log.e(theTag, "onViewCreated: got new state. $it")
            with(binding) {
                if (it.ageData != null) {
                    val nameText = "You, ${it.ageData.name}, are on average, "
                    nameView.text = nameText
                    val ageText = "${it.ageData.age} years old!"
                    ageView.text = ageText
                    val countText = "Based on a sample of ${it.ageData.count} ${it.ageData.name}s."
                    countView.text = countText
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

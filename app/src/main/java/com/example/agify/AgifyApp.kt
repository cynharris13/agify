package com.example.agify

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Agify app.
 *
 * @constructor Create empty Agify app
 */
@HiltAndroidApp
class AgifyApp : Application()

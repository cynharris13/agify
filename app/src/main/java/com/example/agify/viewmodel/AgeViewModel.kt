package com.example.agify.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.agify.model.AgeRepo
import com.example.agify.view.age.AgeState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Age view model.
 *
 * @property repo
 * @constructor Create empty Age view model
 */
@HiltViewModel
class AgeViewModel @Inject constructor(private val repo: AgeRepo) : ViewModel() {
    private val theTag = "AgeViewModel"
    private val _state = MutableLiveData(AgeState())
    val state: LiveData<AgeState> get() = _state
    private val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        Log.e(theTag, "Something went wrong: ", throwable)
    }

    /**
     * Get age.
     *
     * @param name
     */
    fun getAge(name: String) = viewModelScope.launch(exceptionHandler) {
        _state.value = _state.value?.copy(isLoading = true)
        // Log.e(theTag, "getAge: going for the shitz")
        val ageData = repo.getAge(name)
        _state.value = _state.value?.copy(isLoading = false, ageData = ageData)
    }
}

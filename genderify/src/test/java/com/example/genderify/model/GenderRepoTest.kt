package com.example.genderify.model

import com.example.genderify.model.dto.GenderDTO
import com.example.genderify.model.entity.Gender
import com.example.genderify.model.remote.GenderService
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@OptIn(ExperimentalCoroutinesApi::class)
internal class GenderRepoTest {
    // given
    val service = mockk<GenderService>()
    val repo = GenderRepo(service)

    @Test
    fun getGender() = runTest {
        // given
        val dto = GenderDTO(name = "lol")
        coEvery { service.getGender("lol").isSuccessful } coAnswers { true }
        coEvery { service.getGender("lol").body() } coAnswers { dto }
        val expected = Gender(0, "", "lol", 0.0)

        // when
        val actual = repo.getGender("lol")

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun getGenderFail() = runTest {
        // given
        coEvery { service.getGender("lol").isSuccessful } coAnswers { false }
        val expected = Gender(0, "", "", 0.0)

        // when
        val actual = repo.getGender("lol")

        // then
        Assertions.assertEquals(expected, actual)
    }
}

package com.example.genderify.viewmodel

import com.example.genderify.model.GenderRepo
import com.example.genderify.model.entity.Gender
import com.example.genderify.util.CoroutinesTestExtension
import com.example.genderify.util.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class GenderViewModelTest {
    private val repo = mockk<GenderRepo>()
    private val genderVM = GenderViewModel(repo)

    @Test
    fun getGender() = runTest {
        // given
        val expected = Gender(0, "", "lol", 0.0)
        coEvery { repo.getGender("lol") } coAnswers { expected }

        // when
        genderVM.getGender("lol")

        // then
        Assertions.assertFalse(genderVM.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, genderVM.state.value?.gender)
    }
}

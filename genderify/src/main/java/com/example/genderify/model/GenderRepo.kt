package com.example.genderify.model

import com.example.genderify.model.dto.GenderDTO
import com.example.genderify.model.entity.Gender
import com.example.genderify.model.remote.GenderService
import javax.inject.Inject

/**
 * Gender repository.
 *
 * @property genderService to fetch from API
 * @constructor Create empty Gender repo
 */
class GenderRepo @Inject constructor(private val genderService: GenderService) {
    /**
     * Get gender.
     *
     * @param name
     * @return the [Gender] object with name, count, most likely gender, and probability
     */
    suspend fun getGender(name: String): Gender {
        val response = genderService.getGender(name)
        return if (response.isSuccessful) {
            val dto = response.body() ?: GenderDTO()
            with(dto) {
                Gender(count, gender, name, probability)
            }
        } else { Gender(0, "", "", 0.0) }
    }
}

package com.example.genderify.model.entity

/**
 * Gender entity.
 *
 * @property count
 * @property gender
 * @property name
 * @property probability
 * @constructor Create empty Gender
 */
data class Gender(
    val count: Int,
    val gender: String,
    val name: String,
    val probability: Double
)

package com.example.genderify.model.remote

import com.example.genderify.model.dto.GenderDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Gender service.
 *
 * @constructor Create empty Gender service
 */
interface GenderService {
    @GET(".")
    suspend fun getGender(@Query("name") name: String): Response<GenderDTO>
}

package com.example.genderify.model.dto

import kotlinx.serialization.Serializable

@Serializable
data class GenderDTO(
    val count: Int = 0,
    val gender: String = "",
    val name: String = "",
    val probability: Double = 0.0
)

package com.example.genderify

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Gender app.
 *
 * @constructor Create empty Gender app
 */
@HiltAndroidApp
class GenderApp : Application()

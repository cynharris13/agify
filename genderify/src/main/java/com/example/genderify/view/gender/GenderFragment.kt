package com.example.genderify.view.gender

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.genderify.databinding.FragmentSecondBinding
import com.example.genderify.viewmodel.GenderViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@AndroidEntryPoint
class GenderFragment : Fragment() {
    private val genderViewModel by viewModels<GenderViewModel>()

    private val name by lazy { arguments?.getString("name") ?: "" }

    private var _binding: FragmentSecondBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        genderViewModel.getGender(name)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        genderViewModel.state.observe(viewLifecycleOwner) {
            with(binding) {
                if (it.gender != null) {
                    val nameString = "${it.gender.name}, there is a..."
                    nameText.text = nameString
                    val gender = "${(it.gender.probability * PERCENT).toInt()}% chance that you are " +
                        it.gender.gender
                    genderText.text = gender
                    val count = "Out of ${it.gender.count} people who have this name."
                    countText.text = count
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val PERCENT = 100
    }
}

package com.example.genderify.view.gender

import com.example.genderify.model.entity.Gender

/**
 * Gender state.
 *
 * @property isLoading
 * @property gender
 * @constructor Create empty Gender state
 */
data class GenderState(
    val isLoading: Boolean = false,
    val gender: Gender? = null
)

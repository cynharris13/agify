package com.example.genderify.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.genderify.model.GenderRepo
import com.example.genderify.view.gender.GenderState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Gender view model.
 *
 * @property repo
 * @constructor Create empty Gender view model
 */
@HiltViewModel
class GenderViewModel @Inject constructor(private val repo: GenderRepo) : ViewModel() {
    private val _state = MutableLiveData(GenderState())
    val state: LiveData<GenderState> get() = _state

    /**
     * Get gender.
     *
     * @param name
     */
    fun getGender(name: String) = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val gender = repo.getGender(name)
        _state.value = _state.value?.copy(isLoading = false, gender = gender)
    }
}
